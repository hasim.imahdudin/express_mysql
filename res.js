exports.ok = function(values, res){
    const data = {
        status: 200,
        items: values
    };

    res.json(values);
    res.end();
}