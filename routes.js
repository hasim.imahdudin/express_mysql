'use strict';

module.exports =  function(app) {
    const ret = require('./controller')


    app.route('/')
        .get(ret.index)

    app.route('/tampil')
        .get(ret.tampilsemuamahasiswa);

    app.route('/tampil/:id')
        .get(ret.tampilberdasarkanid);
    app.route('/tambah')
        .post(ret.tambahMahasiswa);

    app.route('/ubah')
        .put(ret.ubahMahasiswa);

    app.route('/hapus')
        .delete(ret.hapusMahasiswa);

    app.route('/tampilmatakuliah')
        .get(ret.tampilgroupmatakuliah);
}